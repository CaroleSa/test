#! /usr/bin/env python3
# coding: UTF-8


# imports
import speech_recognition as sr
import pyttsx3

# Initialize the recognizer
r = sr.Recognizer()


# Loop infinitely for user to
# speak
i = 1
while i == 1:

    def robot_speak(text):
        print("Ce que je dis : " + text)
        engine = pyttsx3.init()
        engine.say(text)
        engine.runAndWait()

    # Exception handling to handle
    # exceptions at the runtime
    try:

        # use the microphone as source for input.
        with sr.Microphone() as source:

            # wait for a second to let the recognizer
            # adjust the energy threshold based on
            # the surrounding noise level
            r.adjust_for_ambient_noise(source, duration=0.2)

            # listens for the user's input
            audio = r.listen(source)

            # Using ggogle to recognize audio
            my_text = r.recognize_google(audio, language='fr-FR', show_all=False)
            my_text = my_text.lower()

            if my_text == "stop":
                my_text = "fin du programme"
                i = 0

            robot_speak(my_text)

    except sr.UnknownValueError:
        print("L'audio n'as pas été compris")

    except sr.RequestError as e:
        print("Le service Google Speech API ne fonctionne plus" + format(e))
